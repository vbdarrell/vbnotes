Clients confirmed to support direct playing 4K

Hardware

- [Fire TV Stick 4K]("https://www.amazon.com/Fire-TV-Stick-4K-with-Alexa-Voice-Remote/dp/B079QHML21")
- [Apple TV 4K]("https://www.apple.com/shop/buy-tv/apple-tv-4k")
- [NVIDIA Shield TV]("https://www.amazon.com/NVIDIA-SHIELD-Gaming-Streaming-GeForce/dp/B01N1NT9Y6")
- [Intel NUC]("https://www.amazon.com/Intel-NUC-Mainstream-Kit-NUC8i3BEH/dp/B07GX4X4PW")

Software

- [Infuse for Apple TV]("https://firecore.com/infuse")
- [Plex Kodi Connect]("https://github.com/croneter/PlexKodiConnect/wiki/Installation")
- [Kodi's Plex add-on]("https://forums.plex.tv/t/installation-instructions/168854#latest") with the settings below. The default settings will not work.

*Note:* "Read Rate Too Slow for Continuous Playback" is a poorly worded generic playback error that can occur if your network or hardware does not support 4K.

![](https://i.imgur.com/vvnLwJT.png)
![](https://i.imgur.com/DtqJ3EA.png)